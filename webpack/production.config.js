const path = require('path');

/* eslint-disable import/no-extraneous-dependencies */
const webpack = require('webpack');
const { default: Config } = require('webpack-config');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { StatsWriterPlugin } = require('webpack-stats-plugin');
/* eslint-enable import/no-extraneous-dependencies */

module.exports = new Config().extend('webpack/base.config.js').merge({
  output: {
    filename: '[name].[chunkhash].js',
    chunkFilename: '[name].chunk.[chunkhash].js'
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        include: [path.join(__dirname, '..', 'src', 'client')],
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          allChunks: true,
          use: [
            {
              loader: 'css-loader',
              options: {
                minimize: true,
                sourceMap: true,
                modules: true,
                localIdentName: '[name]-[local]__[hash:base64:5]',
                importLoaders: 1
              }
            },
            {
              loader: 'postcss-loader',
              options: {
                sourceMap: 'inline'
              }
            }
          ]
        })
      }
    ]
  },
  plugins: [
    new ExtractTextPlugin({
      filename: '[name].[contenthash].css'
    }),

    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
        screw_ie8: true,
        conditionals: true,
        unused: true,
        comparisons: true,
        sequences: true,
        dead_code: true,
        evaluate: true,
        if_return: true,
        join_vars: true
      },
      mangle: true,
      output: {
        comments: false,
        beautify: false
      }
    }),

    new StatsWriterPlugin({
      filename: 'webpack-stats.json',
      fields: null
    }),

    new HtmlWebpackPlugin({
      inject: 'head',
      template: path.join(
        __dirname,
        '..',
        'src',
        'client',
        'templates',
        'index.prod.hbs'
      )
    })
  ]
});
