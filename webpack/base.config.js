const path = require('path');

/* eslint-disable import/no-extraneous-dependencies */
const webpack = require('webpack');
const { default: Config } = require('webpack-config');
const CleanWebpackPlugin = require('clean-webpack-plugin');
/* eslint-enable import/no-extraneous-dependencies */

const srcPath = path.join(__dirname, '..', 'src');
const srcClientPath = path.join(srcPath, 'client');
const cleanPaths = [path.join(__dirname, '..', 'dist', process.env.NODE_ENV)];
const cleanOptions = { root: path.join(__dirname, '..') };

module.exports = new Config().merge({
  entry: {
    dropdown: [path.join(srcClientPath, 'index')]
  },
  output: {
    path: path.join(__dirname, '..', 'dist', process.env.NODE_ENV, 'public'),
    publicPath: '/',
    library: 'dropdown',
    libraryTarget: 'window'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        include: [srcPath],
        use: [{ loader: 'babel-loader', options: { cacheDirectory: true } }]
      },

      {
        test: /\.hbs$/,
        use: [
          {
            loader: 'handlebars-loader',
            options: {
              helperDirs: path.join(srcClientPath, 'templates', 'helpers')
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV)
      }
    }),
    new CleanWebpackPlugin(cleanPaths, cleanOptions)
  ]
});
