const path = require('path');

/* eslint-disable import/no-extraneous-dependencies */
const webpack = require('webpack');
const { default: Config } = require('webpack-config');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const WriteFilePlugin = require('write-file-webpack-plugin');
/* eslint-enable import/no-extraneous-dependencies */

module.exports = new Config().extend('webpack/base.config.js').merge({
  devServer: {
    contentBase: path.join(__dirname, '..', 'dist', 'development'),
    port: process.env.DEV_SERVER_PORT
  },
  devtool: 'eval',
  output: {
    filename: '[name].js',
    pathinfo: true
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        include: [path.join(__dirname, '..', 'src', 'client')],
        use: [
          { loader: 'style-loader' },
          {
            loader: 'css-loader',
            options: {
              minimize: false,
              sourceMap: true,
              modules: true,
              localIdentName: '[name]-[local]__[hash:base64:5]',
              importLoaders: 1
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: 'inline'
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.join(
        __dirname,
        '..',
        'src',
        'client',
        'templates',
        'index.dev.hbs'
      ),
      inject: false,
      devServerPort: process.env.DEV_SERVER_PORT
    }),
    new WriteFilePlugin({
      // Write only files that have ".html" and ".js" extensions.
      test: /(\.html|\.js)$/
    })
  ]
});
