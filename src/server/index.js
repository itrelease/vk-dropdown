const path = require('path');
const express = require('express');
const nunjucks = require('nunjucks');
const dotenv = require('dotenv');
const { createLogger } = require('bunyan');

dotenv.config({ path: path.join(__dirname, '.env') });

const apiRoutes = require('./apiRoutes');
const webRoutes = require('./webRoutes');

const logger = createLogger({ name: 'main' });
const isDevelopment = process.env.NODE_ENV === 'development';
const app = express();
const assetsPath = isDevelopment
  ? path.join(__dirname, '..', '..', 'dist', 'development', 'public')
  : path.join(__dirname, 'public');

nunjucks.configure(assetsPath, {
  autoescape: false,
  tags: {
    blockStart: '{%',
    blockEnd: '%}',
    variableStart: '%%',
    variableEnd: '%%',
    commentStart: '<#',
    commentEnd: '#>'
  }
});

app.use(express.static(assetsPath, { index: false }));
app.use('/api', apiRoutes());
app.use('/', webRoutes(process.env.NODE_ENV, { nunjucks }));

function startServer() {
  const server = app.listen(
    Number(process.env.NODEJS_PORT),
    process.env.NODEJS_ADDR,
    () => {
      const { address: host, port } = server.address();

      logger.info(
        `[${process.env.NODE_ENV}] Server listening at http://${host}:${port}`
      );
    }
  );
}

startServer();
