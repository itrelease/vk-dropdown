const express = require('express');
const { createLogger } = require('bunyan');

const dropdownData = require('./data/dropdown');

const logger = createLogger({ name: 'web' });

function responseWithError(status, res, error) {
  res.set({ 'Content-Type': 'text/html' });

  logger.error(error);

  res.status(status).send(error.toString());
}

module.exports = function webRoutes(nodeEnv, { nunjucks }) {
  const router = express.Router();

  router.get('*', (req, res) => {
    logger.info(req.url);

    res.context = {
      body: '',
      dropdownData: JSON.stringify(dropdownData.web)
    };
    console.log('[server] webRoutes.js', { pathname: req.path });

    nunjucks.render('index.html', res.context, (error, data) => {
      if (error) {
        responseWithError(500, res, error);
        return;
      }

      res.status(200).send(data);
    });
  });

  return router;
};
