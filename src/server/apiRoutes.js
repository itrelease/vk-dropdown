const express = require('express');
const { createLogger } = require('bunyan');

const dropdownData = require('./data/dropdown').node;
const buildTrie =
  process.env.NODE_ENV === 'development'
    ? require('../shared/utils/buildTrie')
    : require('./utils/buildTrie');
const transliterate =
  process.env.NODE_ENV === 'development'
    ? require('../shared/utils/transliterate')
    : require('./utils/transliterate');
const convertLayout =
  process.env.NODE_ENV === 'development'
    ? require('../shared/utils/convertLayout')
    : require('./utils/convertLayout');
const getMatchedItemsIds =
  process.env.NODE_ENV === 'development'
    ? require('../shared/utils/getMatchedItemsIds')
    : require('./utils/getMatchedItemsIds');

const logger = createLogger({ name: 'api' });
const dataTrie = Object.keys(dropdownData).reduce((acc, key) => {
  acc[key] = {
    value: '',
    end: false
  };

  return acc;
}, {});
const dataIds = [];
const dataById = Object.keys(dropdownData).reduce((acc, key) => {
  const data = dropdownData[key];

  acc[key] = data.reduce((acc, item) => {
    const words = [item.value.toLowerCase()];
    acc[item.id] = item;

    dataIds.push(item.id);

    if (item.company) {
      words.push(item.company.toLowerCase());
    }

    words.forEach(word => {
      const transliteratedWord = transliterate(word);
      const wordInvertedToLat = convertLayout('ru', word);
      const wordInvertedToCyr = convertLayout('en', transliteratedWord);
      const words = []
        .concat(word.split(' '))
        .concat(transliteratedWord.split(' '))
        .concat(wordInvertedToLat.split(' '))
        .concat(wordInvertedToCyr.split(' '));

      buildTrie(dataTrie[key], { words: words, id: item.id });
    });

    return acc;
  }, {});

  return acc;
}, {});

module.exports = function apiRoutes() {
  const api = express.Router();

  api.get('*', (req, res, next) => {
    logger.info(req.url);

    next();
  });

  api.get('/suggest', (req, res) => {
    if (!dataTrie[req.query.source] || !req.query.value) {
      res.json({ ids: [] });
      return;
    }

    res.json({
      value: req.query.value,
      ids: getMatchedItemsIds(req.query.value, dataTrie[req.query.source])
    });
  });

  api.get('*', (req, res) => {
    res.status(500).send('Error');
  });

  return api;
};
