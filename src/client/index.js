/* eslint-disable no-unused-vars */
import { h, app } from 'hyperapp';
/* eslint-enable no-unused-vars */

import buildTrie from 'src-shared/utils/buildTrie';
import transliterate from 'src-shared/utils/transliterate';
import convertLayout from 'src-shared/utils/convertLayout';
import getMatchedItemsIds from 'src-shared/utils/getMatchedItemsIds';
import fetch from 'src-shared/utils/xhr';
import simpleThrottle from 'src-shared/utils/simpleThrottle';
import debounce from 'src-shared/utils/debounce';

const styles = require('./index.css');

// Polyfill for IE
if (!Object.assign) {
  Object.defineProperty(Object, 'assign', {
    enumerable: false,
    configurable: true,
    writable: true,
    value: function(target) {
      if (target === undefined || target === null) {
        throw new TypeError('Cannot convert first argument to object');
      }

      const to = Object(target);

      for (let i = 1; i < arguments.length; i++) {
        const nextSource = arguments[i];

        if (nextSource === undefined || nextSource === null) {
          continue;
        }

        const keysArray = Object.keys(Object(nextSource));
        for (
          let nextIndex = 0, len = keysArray.length;
          nextIndex < len;
          nextIndex++
        ) {
          const nextKey = keysArray[nextIndex];
          const desc = Object.getOwnPropertyDescriptor(nextSource, nextKey);
          if (desc !== undefined && desc.enumerable) {
            to[nextKey] = nextSource[nextKey];
          }
        }
      }

      return to;
    }
  });
}

const ESC = 27;
const UP = 38;
const DOWN = 40;
const ENTER = 13;

module.exports = function(element, data) {
  const dataTrie = {
    value: '',
    end: false
  };
  const dataIds = [];
  const dataById = data.reduce((acc, item) => {
    acc[item.id] = item;

    dataIds.push(item.id);

    const word = item.value.toLowerCase();
    const transliteratedWord = transliterate(word);
    const wordInvertedToLat = convertLayout('ru', word);
    const wordInvertedToCyr = convertLayout('en', transliteratedWord);
    const words = []
      .concat(word.split(' '))
      .concat(transliteratedWord.split(' '))
      .concat(wordInvertedToLat.split(' '))
      .concat(wordInvertedToCyr.split(' '));

    buildTrie(dataTrie, { words: words, id: item.id });

    return acc;
  }, {});
  const limit = 10;
  const state = {
    opened: false,
    input: '',
    loading: false,
    offset: 1,
    hoveredItemIndex: null,
    suggestedItemsIds: [],
    serverSuggestedItemsIds: [],
    pickedItemsIds: [],
    autocomplete: !!Number(element.getAttribute('data-autocomplete')),
    avatars: !!Number(element.getAttribute('data-avatars')),
    multiselect: !!Number(element.getAttribute('data-multiselect')),
    server: !!Number(element.getAttribute('data-server'))
  };

  // console.log('dropdown', { element, data, dataById, dataTrie }, state);
  const getSuggestFromServer = simpleThrottle((state, actions) => {
    const source = element.getAttribute('data-source');

    fetch('get', `/api/suggest?value=${state.input}&source=${source}`, data => {
      actions.serverSuggest(window.JSON.parse(data));
    });
  }, 500);

  const showMoreSuggest = debounce((state, actions, { element }) => {
    if (element.scrollTop + element.clientHeight + 50 >= element.scrollHeight) {
      actions.showMoreSuggest(state.offset + 1);
    }
  }, 300);

  const actions = {
    serverSuggest: ({ value, ids }) => state =>
      Object.assign({}, state, {
        loading: false,
        serverSuggestedItemsIds:
          value !== state.input || ids.length === 0
            ? []
            : ids.filter(
                id =>
                  state.suggestedItemsIds.indexOf(id) === -1 &&
                  state.pickedItemsIds.indexOf(id) === -1
              )
      }),

    suggestScroll: event => (state, actions) =>
      showMoreSuggest(state, actions, { element: event.target }),

    showMoreSuggest: offset => state => Object.assign({}, state, { offset }),

    openList: () => state => {
      const suggestedItems = state.suggestedItemsIds.concat(
        state.serverSuggestedItemsIds
      );

      // If there is something before close happened
      if (state.input.length > 0) {
        return Object.assign({}, state, {
          opened: true,
          hoveredItemIndex: suggestedItems.length > 0 ? 0 : null
        });
      }

      const suggestedItemsIds = dataIds.filter(
        id => state.pickedItemsIds.indexOf(id) === -1
      );

      return Object.assign({}, state, {
        opened: true,
        suggestedItemsIds: suggestedItemsIds,
        hoveredItemIndex: typeof suggestedItemsIds[0] !== 'undefined' ? 0 : null
      });
    },

    closeList: () => state =>
      Object.assign({}, state, {
        opened: false,
        offest: 1,
        hoveredItemIndex: null
      }),

    overItem: itemIndex => state =>
      Object.assign({}, state, { hoveredItemIndex: itemIndex }),

    pickItem: pickedItemId => state =>
      Object.assign({}, state, {
        input: '',
        opened: false,
        offset: 1,
        hoveredItemIndex: null,
        suggestedItemsIds: [],
        pickedItemsIds: state.pickedItemsIds.concat(pickedItemId)
      }),

    removeItem: itemId => state =>
      Object.assign({}, state, {
        pickedItemsIds: state.pickedItemsIds.filter(id => id !== itemId)
      }),

    keyUp: ({ value, keyCode, target }) => (state, actions) => {
      const newState = Object.assign({}, state, {
        input: value,
        suggestedItemsIds: []
      });

      if (newState.input.length === 0 || !state.autocomplete) {
        // Show all data
        newState.suggestedItemsIds = dataIds;
      } else if (
        // Show autocompleted data
        newState.input.length > 0 &&
        state.autocomplete
      ) {
        newState.suggestedItemsIds = getMatchedItemsIds(
          newState.input,
          dataTrie
        );

        if (state.server && state.input !== newState.input) {
          newState.loading = true;
          getSuggestFromServer(newState, actions);
        }
      }

      // Make suggest list open in case it is not and remove picked items from suggest list
      if (newState.suggestedItemsIds.length > 0) {
        newState.opened = true;
        newState.suggestedItemsIds = newState.suggestedItemsIds.filter(
          id => state.pickedItemsIds.indexOf(id) === -1
        );
      }

      switch (keyCode) {
        case ESC: {
          newState.opened = false;
          target.blur();
          break;
        }

        case UP: {
          newState.hoveredItemIndex =
            state.hoveredItemIndex > 0
              ? state.hoveredItemIndex - 1
              : state.hoveredItemIndex;
          break;
        }

        case DOWN: {
          const suggestedItems = newState.suggestedItemsIds.concat(
            newState.serverSuggestedItemsIds
          );
          newState.hoveredItemIndex = suggestedItems[state.hoveredItemIndex + 1]
            ? state.hoveredItemIndex + 1
            : state.hoveredItemIndex;
          break;
        }

        case ENTER: {
          const suggestedItems = newState.suggestedItemsIds.concat(
            newState.serverSuggestedItemsIds
          );

          if (suggestedItems.length > 0) {
            newState.pickedItemsIds.push(
              suggestedItems[state.hoveredItemIndex]
            );
            newState.opened = false;
            newState.input = '';
            target.blur();
          }
          break;
        }
      }

      if ([UP, DOWN].indexOf(keyCode) > -1) {
        element
          .querySelector(
            `[data-suggest-item-index="${newState.hoveredItemIndex}"]`
          )
          .scrollIntoView();
      }

      return newState;
    }
  };

  /* eslint-disable no-unused-vars */
  const PickedList = ({ pickedItemsIds, onRemoveItem }) => (
    <div class={styles.picked}>
      {pickedItemsIds.map(id => (
        <div key={id} class={styles.pickedItem}>
          {dataById[id].value}
          <span
            class={styles.pickedItemRemove}
            onclick={() => onRemoveItem(id)}
          >
            &times;
          </span>
        </div>
      ))}
    </div>
  );

  const SuggestList = ({
    suggestedItemsIds,
    hoveredItemIndex,
    loading,
    avatars,
    offset,
    onPickItem,
    onOverItem,
    onSuggestScroll
  }) => (
    <div class={styles.list} onscroll={e => onSuggestScroll(e)}>
      {!loading &&
        suggestedItemsIds.length === 0 && (
          <div key="empty" class={styles.listItem}>
            Ничего не найдено :(
          </div>
        )}

      {loading && (
        <div key="loading" class={styles.listItem}>
          Поиск...
        </div>
      )}

      {suggestedItemsIds.slice(0, offset * limit).map((id, index) => {
        const classes = [styles.listItem];

        if (index === hoveredItemIndex) {
          classes.push(styles.listItemHover);
        }

        return (
          <div
            key={id}
            class={classes.join(' ')}
            onmousedown={() => onPickItem(id)}
            onmouseover={() => onOverItem(index)}
            data-suggest-item-index={index}
          >
            {avatars && (
              <img
                class={styles.avatar}
                src={dataById[id].img}
                alt={dataById[id].value}
              />
            )}
            {dataById[id].value}
          </div>
        );
      })}
    </div>
  );
  /* eslint-enable no-unused-vars */

  const view = (state, actions) => {
    const wrapperClasses = [styles.wrapper];

    if (!state.multiselect && state.pickedItemsIds.length > 0) {
      wrapperClasses.push(styles.hidden);
    }

    return (
      <div class={styles.root}>
        {state.pickedItemsIds.length > 0 && (
          <PickedList
            pickedItemsIds={state.pickedItemsIds}
            onRemoveItem={actions.removeItem}
          />
        )}

        <div class={wrapperClasses.join(' ')}>
          <input
            class={styles.input}
            placeholder={element.getAttribute('data-placeholder')}
            value={state.input}
            onfocus={() => actions.openList()}
            onblur={() => actions.closeList()}
            onkeyup={e =>
              actions.keyUp({
                value: e.target.value,
                keyCode: e.keyCode,
                target: e.target
              })
            }
          />
          <div class={styles.rightControl} onclick={() => actions.openList()} />
        </div>

        {state.opened && (
          <SuggestList
            suggestedItemsIds={state.suggestedItemsIds.concat(
              state.serverSuggestedItemsIds
            )}
            loading={state.loading}
            offset={state.offset}
            hoveredItemIndex={state.hoveredItemIndex}
            avatars={state.avatars}
            onPickItem={actions.pickItem}
            onOverItem={actions.overItem}
            onSuggestScroll={actions.suggestScroll}
          />
        )}
      </div>
    );
  };

  app(state, actions, view, element);
};
