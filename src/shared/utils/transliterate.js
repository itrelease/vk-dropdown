const lettersMap = {
  а: 'a',
  б: 'b',
  в: 'v',
  г: 'g',
  д: 'd',
  е: 'e',
  ё: 'e',
  ж: 'zh',
  з: 'z',
  и: 'i',
  й: 'y',
  к: 'k',
  л: 'l',
  м: 'm',
  н: 'n',
  о: 'o',
  п: 'p',
  р: 'r',
  с: 's',
  т: 't',
  у: 'u',
  ф: 'f',
  х: 'kh',
  ц: 'ts',
  ч: 'ch',
  ш: 'sh',
  щ: 'sch',
  ь: '',
  ы: 'y',
  ъ: '',
  э: 'e',
  ю: 'yu',
  я: 'ya'
};

module.exports = function(word) {
  const letters = word.split('');
  const mappedLetters = [];

  for (let i = 0; i < letters.length; i++) {
    const letter = letters[i];

    mappedLetters.push(
      lettersMap[letter.toLowerCase()] || letter.toLowerCase()
    );
  }

  return mappedLetters.join('');
};
