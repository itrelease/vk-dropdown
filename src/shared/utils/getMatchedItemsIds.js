module.exports = function getMatchedItemsIds(string, dataTrie) {
  let curNode = dataTrie;
  const letters = string.split('');

  for (let i = 0; i < letters.length; i++) {
    const lowerCasedLetter = letters[i].toLowerCase();

    if (curNode[lowerCasedLetter]) {
      curNode = curNode[lowerCasedLetter];
    } else {
      curNode = {};
      break;
    }
  }

  return curNode.ids || [];
};
