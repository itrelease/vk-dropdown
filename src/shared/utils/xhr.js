module.exports = (function() {
  const xhr = new window.XMLHttpRequest();

  return function(method, url, callback) {
    xhr.onreadystatechange = function() {
      if (xhr.readyState === 4) {
        callback(xhr.responseText);
      }
    };
    xhr.open(method, url);
    xhr.send();
  };
})();
