module.exports = function buildTrie(trieNode, { words, id }) {
  words.forEach(function(word) {
    const letters = word.split('');
    let curNode = trieNode;

    for (let i = 0; i < letters.length; i++) {
      const letter = letters[i];

      if (typeof curNode[letter] === 'undefined') {
        curNode[letter] = {
          value: letter,
          end: false,
          idsById: { [id]: true },
          ids: [id]
        };
      }

      if (!curNode[letter].idsById[id]) {
        curNode[letter].ids.push(id);
      }

      curNode[letter].idsById[id] = true;

      curNode = curNode[letter];
    }

    curNode.end = true;
  });
};
