module.exports = function throttle(fn, wait) {
  let time = Date.now();

  return function(state, actions, rest) {
    if (time + wait - Date.now() < 0) {
      fn(state, actions, rest);
      time = Date.now();
    }
  };
};
