const en2RuMap = {
  q: 'й',
  w: 'ц',
  e: 'у',
  r: 'к',
  t: 'е',
  y: 'н',
  u: 'г',
  i: 'ш',
  o: 'щ',
  p: 'з',
  '[': 'х',
  '{': 'Х',
  ']': 'ъ',
  '}': 'Ъ',
  '|': '/',
  '`': 'ё',
  '~': 'Ё',
  a: 'ф',
  s: 'ы',
  d: 'в',
  f: 'а',
  g: 'п',
  h: 'р',
  j: 'о',
  k: 'л',
  l: 'д',
  ';': 'ж',
  ':': 'Ж',
  "'": 'э',
  '"': 'Э',
  z: 'я',
  x: 'ч',
  c: 'с',
  v: 'м',
  b: 'и',
  n: 'т',
  m: 'ь',
  ',': 'б',
  '<': 'Б',
  '.': 'ю',
  '>': 'Ю',
  '/': '.',
  '?': ',',
  '@': '"',
  '#': '№',
  $: ';',
  '^': ':',
  '&': '?'
};

const ru2EnMap = {};

for (let prop in en2RuMap) {
  if ({}.hasOwnProperty.call(en2RuMap, prop)) {
    ru2EnMap[en2RuMap[prop]] = prop;
  }
}

module.exports = function(from, word) {
  let lettersMap = from === 'en' ? en2RuMap : ru2EnMap;
  const letters = word.split('');
  const mappedLetters = [];

  for (let i = 0; i < letters.length; i++) {
    const letter = letters[i];

    mappedLetters.push(
      lettersMap[letter.toLowerCase()] || letter.toLowerCase()
    );
  }

  return mappedLetters.join('');
};
