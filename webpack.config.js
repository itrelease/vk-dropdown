const path = require('path');
const { default: Config, environment } = require('webpack-config');
const dotenv = require('dotenv');

dotenv.config({ path: path.join(__dirname, 'src', 'server', '.env') });

environment.setAll({
  env: () => process.env.NODE_ENV
});

const configs = [new Config().extend('webpack/[env].config.js')];

module.exports = configs;
