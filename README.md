## Prerequisites

1. Node >=8.9.x
2. Yarn

## Install

`yarn install`

## Build

`NODE_ENV=production yarn build`

## Run

`cd dist/production` and then `NODE_ENV=production node index.js`
